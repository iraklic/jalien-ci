void die(const char *msg) {
  std::cout << "FAIL:" << msg << std::endl;
  exit(1);
}

void connect_to_jcentral() {
  TGrid::Connect("alien");
  TString host = gGrid->GetHost();

  if(!host.Contains("jcentral")) {
    die("Not connected to jcentral. Stop JBox if it is running locally.");
  }
}

int get_user_test() {
  //connect_to_jcentral();
  TGrid::Connect("alien");

  cout << gGrid->GetHost() << endl;
  const char *user = gGrid->GetUser();
  if(strlen(user) == 0) {
    die("The username returned by gGrid->GetUser() is empty.");
  }

  return 0;
}
