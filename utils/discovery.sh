#!/bin/bash
#. "$TESTS/common.sh"

PATTERN="^[0-9]\{3\}_test"

function jalien_only() {
    [[ -z "${JALIEN}" ]] && echo "Skipping test - jalien only" && exit 0
}

function jbox_only() {
    [[ -z "${JBOX}" ]] && echo "Skipping test - JBox only" && exit 0
}

function replica_only() {
    [[ -z "${REPLICA}" ]] && echo "Skipping test - JCentral replica only" && exit 0
}

export -f jalien_only
export -f jbox_only
export -f replica_only

function collect_tests() {
    local filter="$1"

    if [ "$filter" != "" ]; then
        test_list=`ls $TESTS | grep "$PATTERN" | grep "$filter"`
    else
        test_list=`ls $TESTS | grep "$PATTERN"`
    fi
}

function execute_tests() {
    fresults="$ROOT/test_results.txt"
    echo "test_name,status" > "$fresults"

    for t in $test_list
    do
        pushd $TESTS/$t > /dev/null
        output="$(bash -x ./test.sh 2>&1)"
        status=$?
        popd > /dev/null

        test_name=$(echo $t | sed 's/_/,/')

        if [ $status -ne 0 ]; then
            echo -e "\e[31mTest failed:\e[0m $(basename $t)"
            echo "$test_name,fail" >>"$fresults"
            [ x"${KEEP_GOING}" == x"True" ] && continue
            echo "$output"
            return 1
        else
            echo -e "\e[32mTest passed:\e[0m $(basename $t)"
            echo "$test_name,pass" >> "$fresults"
        fi
    done

    return 0
}

function run_test_suite() {
    local filter="$1"

    collect_tests "$filter"
    execute_tests

    return $?
}
