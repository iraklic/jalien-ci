TGridResult* get_result() {
    TString cmd = "find -l 1  -o 0 /alice/data/2018/LHC18b/000285396 pass1/*ESDs.root";
    TGridResult* res = gGrid->Command(cmd);
    return res;
}

int test_find_api() {
    TGrid::Connect("alien://");
    TGridResult* result = get_result();
    TGridCollection* collection = gGrid->OpenCollectionQuery(result);
    int size = collection->GetNofGroups();
    cout << size << endl;
    delete result;

    if (size == 1) {
        return 0;
    } else {
        return 1;
    }
}
