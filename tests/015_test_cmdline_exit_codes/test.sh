#!/bin/bash
jalien_only

function die() {
    msg=$1
    echo "$msg"
    exit 1
}

function _t() {
    ${@}
    return ${?}
}
function t() {
    _t $@
    status=$?
    [ "$status" -eq "0" ] || die "Test ${cmd} failed"
}

function f() {
    _t $@
    status=$?
    [ "$status" -ne "0" ] || die "Test ${cmd} failed"
}

t alien_ls
f alien_ls doesnotexist

TESTDIR="test_$(date +%s)"
t alien_mkdir "${TESTDIR}"
t alien_rmdir "${TESTDIR}"
f alien_mkdir "/fail/no/permissions/${TESTDIR}"

t alien_stat .
f alien_stat /fail/no/permissions

