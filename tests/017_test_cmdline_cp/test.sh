#!/bin/bash
dst=OCDBFoldervsRange.xml
[ ! $(which alien.py) ] && opts="-t 60"

rm -f $dst
alien_cp -f $opts alien:///alice/data/OCDBFoldervsRunRange.xml file://$dst || exit 1
