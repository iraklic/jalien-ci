#!/bin/bash
set -e
set -x
export PATH=/cvmfs/alice.cern.ch/bin:$PATH
export REPLICA=1

JALIEN_CI_REPO=$PWD
ls

mkdir -p workspace
pushd workspace # START

START="$PWD"
SHARED_VOLUME="$PWD/shared"
mkdir -p $SHARED_VOLUME

# Get the code
set -x
echo "jalien setup repo: ${JALIEN_SETUP_REPO}"
git clone ${JALIEN_SETUP_REPO:-https://gitlab.cern.ch/jalien/jalien-setup.git} jalien-setup -b ${JALIEN_SETUP_BRANCH:-master}
git clone --depth 1   ${JALIEN_REPO:-https://gitlab.cern.ch/jalien/jalien.git} jalien       -b ${JALIEN_BRANCH:-master}
set +x

# Build jalien
pushd jalien
    ./compile.sh cs
    JAR_PATH=$PWD/alien-cs.jar
popd

# Start service
pushd jalien-setup
    make all
    set -x
    ./bin/jared --jar $JAR_PATH --volume $SHARED_VOLUME
    cd $SHARED_VOLUME
    cat .env
    cat env_setup.sh
    cat docker-compose.yml
    set +x
    docker-compose config
    docker-compose up -d
popd

# Run the tests
which alienv
# LATEST_JALIEN_ROOT=$(alienv q | grep -i jalien-root | sort -g | tail -n 1)

# TODO: set the environment in this shell
alienv setenv JAliEn-ROOT/0.5.5-25 -c ". $SHARED_VOLUME/env_setup.sh; cd $JALIEN_CI_REPO; while ! alien.py pwd; do sleep 2; done; ./integration_tests.sh --keep-going"

# Stop the service
pushd $SHARED_VOLUME
  docker-compose down -t 2
popd

popd # END
rm -rf workspace || true
